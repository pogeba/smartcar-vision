QT += core
QT -= gui
QT += network

TARGET = smartcar-vision
CONFIG += console
CONFIG -= app_bundle

CONFIG += C++11

TEMPLATE = app

SOURCES += main.cpp \
    dector.cpp \
    posix_qextserialport.cpp \
    qextserialbase.cpp \
    serial.cpp \
    socketclient.cpp

HEADERS += \
    dector.h \
    posix_qextserialport.h \
    qextserialbase.h \
    serial.h \
    socketclient.h

INCLUDEPATH += /usr/include \
/usr/local/include/opencv \
/usr/local/include/opencv2

LIBS += /usr/local/lib/libopencv_calib3d.so \
/usr/local/lib/libopencv_core.so \
/usr/local/lib/libopencv_features2d.so \
/usr/local/lib/libopencv_flann.so \
/usr/local/lib/libopencv_highgui.so \
/usr/local/lib/libopencv_imgcodecs.so \
/usr/local/lib/libopencv_imgproc.so \
/usr/local/lib/libopencv_ml.so \
/usr/local/lib/libopencv_objdetect.so \
/usr/local/lib/libopencv_photo.so \
/usr/local/lib/libopencv_shape.so \
/usr/local/lib/libopencv_stitching.so \
/usr/local/lib/libopencv_superres.so \
/usr/local/lib/libopencv_videoio.so \
/usr/local/lib/libopencv_video.so \
/usr/local/lib/libopencv_videostab.so

#INCLUDEPATH += C:/opencv/build/include\
#               C:/opencv/build/include/opencv\
#               C:/opencv/build/include/opencv2

#LIBS +=-L C:/opencv/MinGW_build/lib/libopencv_*.a
